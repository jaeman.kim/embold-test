const DEFAULT_CUSTOMER_L2_LIST = [
  {
    name: "thingplus.page.entire-view.entire-customer",
    label: "thingplus.page.entire-view.entire-customer",
    id: { id: "", entityType: "" },
  },
];

self.onInit = function () {
  const timestampYearMonthDateFormat = translate.instant(
    "thingplus.time-format.ymd-2"
  );
  const timestampHourMinuteFormat = translate.instant(
    "thingplus.time-format.hm-1"
  );
  const dayTranslationKey = `thingplus.time-format.${moment()
    .format("dddd")
    .toLowerCase()}`;

  self.ctx.custom = {};
  let { custom, $container, $scope, translate, stateController } = self.ctx;

  $scope.title = "thingplus.page.entire-view.real-time-robot-status";

  Object.assign(custom, getCustomVariables({ $container }));

  const timestampDay = translate.instant(dayTranslationKey);

  $scope.timestamp = getTimestampWithMoment({
    timestampYearMonthDateFormat,
    timestampHourMinuteFormat,
    timestampDay,
  });
  custom.intervalFunc = setInterval(() => {
    $scope.timestamp = getTimestampWithMoment({
      timestampYearMonthDateFormat,
      timestampHourMinuteFormat,
      timestampDay,
    });
    self.ctx.detectChanges();
  }, 60 * 1000);

  const customerL2List = [
    ...DEFAULT_CUSTOMER_L2_LIST,
    ...self.ctx.defaultSubscription.datasources.map(
      (datasource) => datasource.entity
    ),
  ];
  $scope.customerL2List = customerL2List;
  $scope.selectedCustomerL2 = getSelectedCustomerL2FromDashboardParams({
    stateController,
  });

  $scope.handleChangeEntitySelect = ($event) =>
    updateDashboardParams({ $event, stateController, customerL2List });

  $scope.handleClickRefreshButton = () => self.ctx.updateAliases();

  self.onResize();
  $scope.isInit = true;
};

function getCustomVariables({ $container }) {
  const $wrapper = $(".wrapper", $container);

  return { $wrapper };
}

function getTimestampWithMoment({
  timestampYearMonthDateFormat,
  timestampHourMinuteFormat,
  timestampDay,
}) {
  const timestampYearMonthDate = moment().format(timestampYearMonthDateFormat);
  const timestampHourMinute = moment().format(timestampHourMinuteFormat);
  return `${timestampYearMonthDate} ${timestampDay} ${timestampHourMinute}`;
}

function getSelectedCustomerL2FromDashboardParams({ stateController }) {
  if (!stateController) return;

  const params = stateController.getStateParams();
  if (params.entityId) return params.entityId.id;
  return "";
}

function updateDashboardParams({ $event, stateController, customerL2List }) {
  const newSelectedCustomerL2 = customerL2List.find(
    (customer) => customer.id.id === $event
  );
  const params = {
    entityId: newSelectedCustomerL2.id,
    entityName: newSelectedCustomerL2.name,
    entityLabel: newSelectedCustomerL2.label,
  };
  return stateController.updateState("default", params, null);
}

self.onDataUpdated = function () {};

self.onResize = function () {
  let { settings, isMobile, width, custom } = self.ctx;

  const fontSize = getWidgetFontSizeWithThrottle({ settings, isMobile, width });
  if (custom.$wrapper) custom.$wrapper.css("font-size", fontSize);
};

const getWidgetFontSizeWithThrottle = _.throttle(getWidgetFontSize, 200, {
  trailing: true,
});

function getWidgetFontSize({ settings, isMobile, width }) {
  let originWidth = settings.widget.originWidth;
  if (isMobile) originWidth = 960;

  let widgetFontSize = _.round((width / originWidth) * 10, 2);
  if (widgetFontSize < 6.25) widgetFontSize = 6.25;

  return `${widgetFontSize}px`;
}

self.onDestroy = function () {
  let { custom } = self.ctx;
  clearInterval(custom.intervalFunc);
};

self.typeParameters = function () {
  return {
    maxDatasources: -1,
    maxDataKeys: -1,
    dataKeysOptional: true,
  };
};
